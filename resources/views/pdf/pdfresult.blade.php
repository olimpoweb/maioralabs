@extends('layout.layoutpdf')
@section('content')
<div class="content container">

    <div class="list-container w-auto mt-3 align-content-start">
        <div class="row">
            <div class="col-12">
                <h2>Riepilo Importazione</h2>
                <h5>Sono stati importati {{ $ImportedFiles['result'] }} Brani - Data Importazione {{ date('d-m-Y H:i:s', strtotime($ImportedFiles['created_at'])) }}</h5>
                <h5></h5>
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">Codice</th>
                        <th scope="col">Brano</th>
                        <th scope="col">Autore</th>
                        <th scope="col">Durata</th>
                        <th scope="col">Genere</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ImportedFiles->importedSong as $item)
                            <tr>
                            <td>{{ $item['codice'] }}</td>
                            <td>{{ $item['brano'] }}</td>
                            <td>{{ $item['artista'] }}</td>
                            <td>{{ $item['durata'] }}</td>
                            <td>{{ $item['genere'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@endsection
