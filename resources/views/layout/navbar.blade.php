<nav class="navbar sticky-top navbar-light bg-light">
    @foreach ($slot as $item)
        <a class="nav-link" href="{{ route($item['route']) }}">{{ $item['name'] }}</a>
    @endforeach
</nav>
