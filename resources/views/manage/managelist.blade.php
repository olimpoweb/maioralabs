@extends('layout.layout')
@include('layout.navbar', ['slot' =>[['route' => 'manage.add', 'name' => 'Nuova Richiesta'], ['route' => 'home', 'name' => 'Home']]])
@section('content')
<div class="content container">


    <div class="list-container w-auto mt-3 align-content-start">
        <div class="row">
            <div class="col-12">
            <h2>Ultima Importazione</h2>
            <h5>Sono stati importati {{ $lastImport['result'] }} Brani</h5>
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Codice</th>
                    <th scope="col">Brano</th>
                    <th scope="col">Autore</th>
                    <th scope="col">Durata</th>
                    <th scope="col">Genere</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($lastImport->importedSong as $item)
                        <tr>
                        <td>{{ $item['codice'] }}</td>
                        <td>{{ $item['brano'] }}</td>
                        <td>{{ $item['artista'] }}</td>
                        <td>{{ $item['durata'] }}</td>
                        <td>{{ $item['genere'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-12">
            <h2>Importazioni precedenti</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">File Originale</th>
                        <th scope="col">Data Importazione</th>
                        <th scope="col">Risultato</th>
                        <th scope="col">PDF</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($requestSong->importedFiles as $item)
                        <tr>
                        <td>{{ $item['id'] }}</td>
                        <td>{{ $item['filename'] }}</td>
                        <td>{{ date('d-m-Y H:i:s', strtotime($item['created_at'])) }}</td>
                        <td class="@if($item['status'] == 1)
                        bg-success
                        @elseif($item['status'] == 2)
                        bg-warning
                        @else
                        bg-danger
                        @endif">{{ $item['result'] }}</td>
                        <td><a href="{{action('ManageController@createPDF', $item['id'])}}">PDF</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>

    </div>
</div>
@endsection
