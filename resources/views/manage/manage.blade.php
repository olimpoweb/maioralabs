@extends('layout.layout')
@section('content')
<div class="content container">

    <div class="form-container w-auto mt-3 align-content-start">
        <h2>Crea una nuova richiesta di registrazione</h2>
        <form-insert inline-template route="{{ route('api.manage.store') }}">
            <form @submit="_submitRequest()">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nome">Nome</label>
                        <input type="text" v-model="form.nome" class="form-control" id="nome" placeholder="Nome">
                        <div class="text-danger" v-if="post.errors.nome">@{{ post.errors.nome[0] }}</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="cognome">Cognome</label>
                        <input type="text" v-model="form.cognome" class="form-control" id="cognome" required placeholder="Cognome">
                        <div class="text-danger" v-if="post.errors.cognome">@{{ post.errors.cognome[0] }}</div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="codice-fiscale">Codice Fiscale</label>
                        <input type="text" v-model="form.codice_fiscale" class="form-control" id="codice-fiscale" required placeholder="Codice Fiscale">
                        <div class="text-danger" v-if="post.errors.codice_fiscale">@{{ post.errors.codice_fiscale[0] }}</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telefono">Telefono</label>
                        <input type="text" v-model="form.telefono" class="form-control" id="telefono" required placeholder="Telefono">
                        <div class="text-danger" v-if="post.errors.telefono">@{{ post.errors.telefono[0] }}</div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="email">Email</label>
                        <input type="email" v-model="form.email" class="form-control" id="email" required placeholder="Email">
                        <div class="text-danger" v-if="post.errors.email">@{{ post.errors.email[0] }}</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="indirizzo">Indirizzo</label>
                        <input type="text" v-model="form.indirizzo" class="form-control" id="indirizzo" placeholder="Indirizzo">
                        <div class="text-danger" v-if="post.errors.indirizzo">@{{ post.errors.indirizzo[0] }}</div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-12">
                        <label for="note">Note</label>
                        <textarea class="form-control" v-model="form.note" id="note" rows="3"></textarea>
                        <div class="text-danger" v-if="post.errors.note">@{{ post.errors.note[0] }}</div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <label for="upload">Carica file xls</label>
                        <input type="file" v-on:change="onFileChange" class="form-control-file" required id="upload">
                        <div class="text-danger" v-if="post.errors.file">@{{ post.errors.file[0] }}</div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary" @click.prevent="_submitRequest()">Crea richiesta</button>
            </form>
        </form-insert>

    </div>
</div>
@endsection
