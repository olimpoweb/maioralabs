<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFKtoImportedSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('imported_songs', function (Blueprint $table) {
            $table->foreign('imported_files_id')->references('id')->on('imported_files')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('imported_songs', function (Blueprint $table) {
            $table->dropForeign('imported_files_id');
        });
    }
}
