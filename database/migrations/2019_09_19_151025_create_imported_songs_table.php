<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportedSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imported_songs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('imported_files_id')->index();
            $table->string('codice', 50)->index();
            $table->string('artista');
            $table->string('brano');
            $table->string('durata', 50);
            $table->string('genere');
            $table->timestamps();

            $table->index(['codice', 'imported_files_id'])->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imported_songs');
    }
}
