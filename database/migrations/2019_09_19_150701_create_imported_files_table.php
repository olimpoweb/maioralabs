<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportedFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imported_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('song_request_id')->index();
            $table->string('filename');
            $table->string('uploaded');
            $table->string('result')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('song_request_id')->references('id')->on('song_request')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imported_files');
    }
}
