<p align="center"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjTnsYQFgQc9GpYgxB3D7Ym3sHDhHn_XM4ZURf0HmCv5vICH-hZQ" width="400"></p>

## Installazione

```console
composer install
```
```console
npm install
```

## Considerazioni

Non mi era chiaro come poter andare un update riguardo al caricamento dei brani da XLS, ho pensato che si poteva agire creando un unique id sul record creando un index idRequest+codiceBrano però poi non avrei potuto collegare i file importati allerelative righe, inquanto si andava a sovrascrivere l'index di riferimento. Si sarebbe potuta creare una tabella di appoggio dove venivano salvate le righe importate collegandole al file importato ed una tabella che conteneva tutti gli inserimenti questa volta univoci e con possibilità di update, però diventava una cosa lunga.

Avrei voluto implementare una rules sulla validazione per il codice_fiscale ed sviluppare meglio il package Excel che ho tutlizzato soprattutto lato validazione.

Per quanto riguarda il codice_fiscale avevate scritto di mettere 11 caratteri, in realtà dovrebbero essere 16, 11 invece la partita iva per questo ho messo 16 must come validazione. Anche qui una validazione sulla forma sarebbe stata perfetta

Altra cosa che sarebbe stata bella inserire le google_place_api in autocomplete per l'indirizzo.

Anche il codice di solito lo commento molto di più

Se volete posso completarlo con quanto detto sopra senza nessun problema. Purtroppo il temp onon mi ha permesso di farlo :(
