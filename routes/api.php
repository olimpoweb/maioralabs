<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/manage', 'ManageController@store')->name('api.manage.store');
Route::put('/manage', 'ManageController@update')->name('api.manage.update');
Route::delete('/manage', 'ManageController@delete')->name('api.manage.delete');
