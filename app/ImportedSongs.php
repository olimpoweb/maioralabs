<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportedSongs extends Model
{
    //
    protected $table = 'imported_songs';

    protected $guarded = [];
}
