<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\SongRequest;
use App\ImportedSongs;
use App\ImportedFiles;
use App\Imports\ImportedSongsImport;
use Excel;
use Illuminate\Support\Facades\Input;
use PDF;

class ManageController extends Controller
{
    //
    public function show()
    {
        return view('manage.manage');
    }

    public function store(Request $request)
    {

        $values = $request->all();

        $this->validate($request, [
            'nome' => 'required|max:100',
            'cognome' => 'required|max:100',
            'telefono' => 'required|max:30',
            'email' => 'required|email',
            'codice_fiscale' => 'required|size:16',
            'indirizzo' => 'required',
            'file' => 'required|mimes:xls'
        ]);

        $fileName = time().'.'.$request->file->getClientOriginalExtension();
        $array = Excel::toArray(new ImportedSongsImport, $request->file);

        if(count($array[0]) > 1) {

            $values['file'] = $fileName;
            $email = $values['email'];

            $SongRequest = SongRequest::updateOrCreate(['email' => $email], $values);

            $ImportedFile = $SongRequest->importedFiles()->create(['uploaded' => $fileName, 'filename' => $request->file->getClientOriginalName(), 'status' => 1]);
            $array_keys = array('codice', 'artista', 'brano', 'durata', 'genere');
            $imported = 0;

            foreach ($array[0] as $key => $value) {
                if($key > 0) {
                    $imported++;
                    try {
                        $result = $ImportedFile->importedSong()->create(array_combine($array_keys,$value));
                    } catch (\Throwable $th) {
                        $imported--;
                        $ImportedFile->status = 2;
                        $ImportedFile->save();

                    }

                }
            }
            $request->file->move(public_path('upload/'.$SongRequest->id), $fileName);

            $ImportedFile->result = $imported.' di '.(count($array[0])-1);
            $ImportedFile->save();
            return response()->json(['result' => $SongRequest, 'rows' => count($array[0])-1, 'imported' => $imported, 'last' => $ImportedFile], 200);

        } else {
            $SongRequest = SongRequest::updateOrCreate(['email' => $email], $values);

            $ImportedFile = $SongRequest->importedFiles()->create(['uploaded' => $fileName, 'filename' => $request->file->getClientOriginalName()]);
            $ImportedFile->result = '0 di 0';
            $ImportedFile->save();

            $request->file->move(public_path('upload/'.$SongRequest->id), $fileName);

            return response()->json(['error' => 'Nessun elemento da importare', 'result' => $ImportedFile], 410);
        }

    }

    public function list(Request $request)
    {
        $SongRequest = SongRequest::findOrFail($request->id);

        $lastImport = $SongRequest->ImportedFiles->first();

        return view('manage.managelist')->with(['requestSong' => $SongRequest, 'lastImport' => $lastImport]);
    }

    public function createPDF($id){

        $ImportedFiles = ImportedFiles::findOrFail($id);

        $pdf = PDF::loadView('pdf.pdfresult', compact('ImportedFiles'));
        return $pdf->download('export.pdf');

    }
}
