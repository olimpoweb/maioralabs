<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportedFiles extends Model
{
    protected $table = 'imported_files';

    protected $guarded = [];

    public function importedSong()
    {
        return $this->hasMany('App\ImportedSongs');
    }

}
