<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongRequest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'song_request';

    protected $guarded = [];


    public function importedFiles()
    {
        return $this->hasMany('App\ImportedFiles')->orderBy('id', 'desc');;
    }
}
