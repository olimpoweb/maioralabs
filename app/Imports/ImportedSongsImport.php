<?php

namespace App\Imports;

use App\ImportedSongs;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportedSongsImport implements ToModel
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ImportedSongs([
            'codice' => $row[0],
            'artista' => $row[1],
            'brano'  => $row[2],
            'durata' => $row[3],
            'genere' => $row[4]
        ]);
    }

}
